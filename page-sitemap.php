<?php 
/*
 * Sitemap
*/
get_header(); ?>

    
<?php  
$headerimage = get_post_meta( $post->ID, 'headerimage', true );
?>
<style>
#header-image { margin-top: -50px; background-image:url('<?php bloginfo('stylesheet_directory'); ?>/<?php echo $headerimage; ?>'); height:300px; background-size:cover; }
</style>
<div id="header-image">
<div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span><?php the_title(); ?></span></h1>
          </div>
        </div>
</div>
    <div class="container">
      <div class="row">
        <div class="col-md-8"><!-- left main content -->

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<div class="row">
    <div class="col-sm-11">
        <div class="block-header">
            <h2>
            <i class="fa fa-globe"></i> <span class="title"><?php echo get_post_meta( $post->ID, 'h2', true );?></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            </h2>
        </div>
    </div>
</div>
<?php the_content(); ?>

<?php endwhile; endif; ?>
    <div class="row">
 <h2>Recent Posts</h2>
<ul>
<?php
	$args = array( 'numberposts' => '5' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
	}
?>
</ul>
    </div>
</div><!-- /left main content -->
<?php get_sidebar(); ?>
    </div><!-- /container (page) -->
</div><!-- /wrapper -->
<?php get_footer(); ?>
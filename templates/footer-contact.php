<?php global $post; ?>

<?php
//Alexandria
if (is_page( '86' ) || '86' == $post->post_parent) { ?>
<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">3600 Jackson Street Ext #201 Dunbar Plaza</span><br />
     <span itemprop="addressLocality">Alexandria</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">71303</span>
   </div>
   Phone: <span itemprop="telephone">318-416-0089</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>


<?php }
// New Orleans
elseif(is_page( '25' ) || '25' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">909 Poydras Street Suite 2000</span><br />
     <span itemprop="addressLocality">New Orleans</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">70112</span>
   </div>
   Phone: <span itemprop="telephone">1-800-537-8185</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>



<?php } 
// Baton Rouge
elseif(is_page( '29' ) || '29' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">5555 Hilton #101</span><br />
     <span itemprop="addressLocality">Baton Rouge</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">70808</span>
   </div>
   Phone: <span itemprop="telephone">(225) 341-5694</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>



<?php }
// Shreveport 
elseif(is_page( '54' ) || '54' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">401 Edwards St #800</span><br />
     <span itemprop="addressLocality">Shreveport</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">71101</span>
   </div>
   Phone: <span itemprop="telephone">(318) 553-5470</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php } 
// Lafayette
elseif(is_page( '74' ) || '74' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">4021 Ambassador Caffery Pkwy</span><br />
     <span itemprop="addressLocality">Lafayette</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">70503</span>
   </div>
   Phone: <span itemprop="telephone">337-446-4571</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php } 
// Lake Charles
elseif(is_page( '96' ) || '96' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">1 Lakeshore Dr</span><br />
     <span itemprop="addressLocality">Lake Charles</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">70629</span>
   </div>
   Phone: <span itemprop="telephone">337-377-0514</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php } 
// Monroe
elseif(is_page( '135' ) || '135' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">1900 N. 18th St</span><br />
     <span itemprop="addressLocality">Monroe</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">71201</span>
   </div>
   Phone: <span itemprop="telephone">(318) 884-0904</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php } 
// Biloxi
elseif(is_page( '147' ) || '147' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">1907 Pass Rd</span><br />
     <span itemprop="addressLocality">Biloxi</span>, <span itemprop="addressRegion">MS</span> <span itemprop="postalCode">39531</span>
   </div>
   Phone: <span itemprop="telephone">(228) 207-5266</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>


<?php } 
// Gulfport 154
elseif(is_page( '154' ) || '154' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">1712 15th St #200</span><br />
     <span itemprop="addressLocality">Gulfport</span>, <span itemprop="addressRegion">MS</span> <span itemprop="postalCode">39501</span>
   </div>
   Phone: <span itemprop="telephone">(228) 207-5266</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>


<?php } 
// Mobile
elseif(is_page( '182' ) || '182' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">63 S Royal St #902</span><br />
     <span itemprop="addressLocality">Mobile</span>, <span itemprop="addressRegion">AL</span> <span itemprop="postalCode">36602</span>
   </div>
   Phone: <span itemprop="telephone">(251) 298-8380</span><br />
   Author: <a href="<?php if(is_page('182')){ ?> https://plus.google.com/b/115091113322685916113/115091113322685916113 <?php } else { ?> https://plus.google.com/112883956779075172119/posts <?php } ?>" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php }
// Texarkana
elseif(is_page( '196' ) || '196' == $post->post_parent) { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">210 N State Line Avenue #501</span><br />
     <span itemprop="addressLocality">Texarkana</span>, <span itemprop="addressRegion">AR</span> <span itemprop="postalCode">71854</span>
   </div>
   Phone: <span itemprop="telephone">(870) 330-4155</span><br />
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php } else { ?>

<div itemscope itemtype="http://schema.org/Attorney">
   <span itemprop="name">Morris Bart and Associates</span><br />
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">909 Poydras Street Suite 2000</span><br />
     <span itemprop="addressLocality">New Orleans</span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">70112</span>
   </div>
   <?php 
   /*
   8726 - Car Accident Attorney Louisiana
8728 - Personal Injury Attorney Louisiana
   */
   ?>
   <?php if(is_page(8726) || is_page(8728)): ?>
   Phone: <span itemprop="telephone">1-800-818-0934</span><br />
   <?php else: ?>
   Phone: <span itemprop="telephone">1-800-537-8185</span><br />
   <?php endif; ?>
   Author: <a href="https://plus.google.com/112883956779075172119/posts" rel="author"><span class="fn">Morris Bart</span></a>
</div>

<?php } ?>

<div id="wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <h1 class="animated slideInDown">Attorneys and Trial Lawyers</h1>
            <div class="list">
              <ul>
                <li class="animated slideInLeft first"><span><i class="fa fa-tachometer"></i> Auto Accidents and Personal Injury </span></li>
                <li class="animated slideInLeft second"><span><i class="fa fa-phone-square"></i>  1-800-537-8185 </span></li>
		<!-- Button trigger modal -->
		<button class="animated slideInLeft third btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
  		<i class="fa fa-hand-o-right fa-3"></i> CLICK TO CONTACT AN ATTORNEY!
		</button>
              </ul>
            </div>
          </div>
          <div class="col-md-6 hidden-sm hidden-xs">
            <div class="showcase">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/img/Morris-Bart.png" alt="Morris Bart" class="iMac animated fadeInDown">
            </div>
          </div>
        </div>
      </div>
    </div>
<?php global $post; ?>

<?php
//Alexandria
if (is_page( '86' ) || '86' == $post->post_parent) { ?>
<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'alexandria',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>
<?php }
// New Orleans
elseif(is_page( '25' ) || '25' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'new-orleans',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Baton Rouge
elseif(is_page( '29' ) || '29' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'baton-rouge',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php }
// Shreveport 
elseif(is_page( '54' ) || '54' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'shreveport',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Lafayette
elseif(is_page( '74' ) || '74' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'lafayette',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Alexandria
elseif(is_page( '86' ) || '86' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'alexandria',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Lake Charles
elseif(is_page( '96' ) || '96' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'lake-charles',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Monroe
elseif(is_page( '135' ) || '135' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'monroe',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Biloxi
elseif(is_page( '147' ) || '147' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'biloxi',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Gulfport 154
elseif(is_page( '154' ) || '154' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'gulfport',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } 
// Mobile
elseif(is_page( '182' ) || '182' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'mobile',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php }
// Texarkana
elseif(is_page( '196' ) || '196' == $post->post_parent) { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
        'menu' => 'texarkana',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>
<?php } else { ?>

<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Morris Bart</a>
        </div>
   <?php wp_nav_menu( array( 
	'container' => 'div',
	'container_class' => 'collapse navbar-collapse',
	'theme_location' => 'main-menu',
	'menu_class' => 'nav navbar-nav',
	'depth' => 2,
	'walker' => new Bootstrap_Walker(),									
	) );
    ?>
	</div>
</div>

<?php } ?>
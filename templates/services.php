<style>
.services li {
position:relative;
}
.servicestext {
color:#eee;
position:absolute;
top:10px;
right:5px;
}
.servicestext:hover {
color:#fff;
}
</style>
<div class="row hidden-xs">
        <div class="col-md-12">
          <div class="services">
            <ul>
                <li>
                  <p><img src="<?php bloginfo('stylesheet_directory'); ?>/img/nblsc.png" class="img-responsive"><span class="servicestext">American Society of Legal Advocates</span><p>
                </li>
                <li>
                  <p><img src="<?php bloginfo('stylesheet_directory'); ?>/img/nationaltriallawyers.png" class="img-responsive"><span class="servicestext">National Trial Lawyers</span><p>
                </li>
                <li>
                 <p> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/asla2.png" class="img-responsive"><span class="servicestext">ASLA</span><p>
                </li>
                <li>
                 <p> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/super-lawyers.png" class="img-responsive"><span class="servicestext">Super Lawyers</span><p>
                </li>
                <li>
                 <p> <img src="<?php bloginfo('stylesheet_directory'); ?>/img/milliondollar.png" class="img-responsive"><span class="servicestext">Million Dollar<br /> Advocate Forum</span><p>
                </li>
            </ul>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
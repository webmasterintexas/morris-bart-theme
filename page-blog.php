<?php get_header(); ?>





    <div class="section-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span>Morris Bart Blog</span></h1>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-sm-8 blog-summary-list">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <!-- blog post -->
            <div class="blog-summary">
              <h4 class="new-story entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
              <ul class="text-muted list-inline">
                <li><i class="fa fa-user"></i> <span class="vcard author"><span class="fn"><?php the_author(); ?></span></span></li>
                <li><i class="fa fa-calendar date updated"></i> <?php the_date(); ?></li>
                <li><i class="fa fa-comments-o"></i> <?php wp_list_comments( array( 'style' => 'ul' ) ); ?></li>
              </ul>
              <hr>
              <p class="blog-text">
                <img class="pull-right img-responsive blog-img" alt="Blog Image" src="the_post_thumbnail('thumbnail');">
                <?php the_excerpt(); ?>
              </p>
              <p class="tags">
                <a href="#"><?php the_tags(); ?></a>
              </p>
<?php endwhile; endif; ?>
            </div>
 
          <!-- Pagination -->
          <ul class="pagination pull-right">
            <li class="disabled"><a href="#">&laquo;</a></li>
            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
          </ul>
        </div>
        <div class="col-sm-4">
          <!-- Social Links -->
          <h3 class="hl">Bookmark</h3>
          <hr>
          <div class="social-icons social-icons-default">
            <ul>
                <li class="rss"><a href="#"><i class="fa fa-rss"></i></a></li>
                <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li class="github"><a href="#"><i class="fa fa-github"></i></a></li>
                <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li class="vk"><a href="#"><i class="fa fa-vk"></i></a></li>
                <li class="plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li class="dropbox"><a href="#"><i class="fa fa-dropbox"></i></a></li>
                <li class="vimeo"><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
          
          <!-- Recently Added -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Recently Added</h3>
            </div>
            <div class="panel-body">
              <div class="recent-blogs">
                <ul>
                  <li><a href="blog-post.html" class="new-story">HighLand - a new responsive Bootstrap 3 template</a></li>
                  <li><a href="blog-post.html" class="new-story">Lorem ipsum dolor sit amet, consectetur adipiscing elit</a></li>
                  <li><a href="blog-post.html">Nulla dapibus lorem sed felis placerat fringilla</a></li>
                  <li><a href="blog-post.html">Praesent eget nibh quis arcu iaculis commodo et at lectus</a></li>
                  <li><a href="blog-post.html">Etiam a enim ut odio laoreet viverra</a></li>
                </ul>
              </div>
            </div>
          </div>

          <!-- Tags -->
          <h3 class="hl">Popular Tags</h3>
          <hr>
          <p class="tags">
            <a href="#">Bootstrap</a>
            <a href="#">lorem</a>
            <a href="#">ipsum</a>
            <a href="#">dolor</a>
            <a href="#">sit</a>
            <a href="#">amet</a>
            <a href="#">consectetur</a>
            <a href="#">elit</a>
            <a href="#">Nulla</a>
            <a href="#">dapibus</a>
            <a href="#">lorem</a>
            <a href="#">placerat</a>
            <a href="#">fringilla</a>
            <a href="#">Praesent</a>
            <a href="#">eget</a>
            <a href="#">nibh</a>
            <a href="#">quis</a>
            <a href="#">arcu</a>
            <a href="#">iaculis</a>
            <a href="#">commodo</a>
          </p>
          <br />
          
          <!-- Archive -->
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Archive</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-12">
                  <!-- Years -->
                  <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#2013" data-toggle="tab">2013</a></li>
                    <li><a href="#2012" data-toggle="tab">2012</a></li>
                    <li><a href="#2011" data-toggle="tab">2011</a></li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <!-- Months -->
                  <div class="tab-content blog-months">
                    <div class="tab-pane active" id="2013">
                      <ul class="nav nav-pills nav-stacked pull-left">
                        <li><a href="#">Jan <span class="badge">15</span></a></li>
                        <li><a href="#">Feb <span class="badge">23</span></a></li>
                        <li><a href="#">Mar <span class="badge">45</span></a></li>
                        <li><a href="#">Apr <span class="badge">12</span></a></li>
                        <li><a href="#">May <span class="badge">10</span></a></li>
                        <li><a href="#">Jun <span class="badge">9</span></a></li>
                      </ul>
                      <ul class="nav nav-pills nav-stacked pull-left">
                        <li><a href="#">Jul <span class="badge">14</span></a></li>
                        <li><a href="#">Aug <span class="badge">19</span></a></li>
                        <li><a href="#">Sep <span class="badge">32</span></a></li>
                        <li><a href="#">Oct <span class="badge">23</span></a></li>
                        <li><a href="#">Nov <span class="badge">8</span></a></li>
                        <li><a href="#">Dec <span class="badge">9</span></a></li>
                      </ul>
                    </div>
                    <div class="tab-pane" id="2012">
                      <ul class="nav nav-pills nav-stacked pull-left">
                        <li><a href="#">Jan <span class="badge">10</span></a></li>
                        <li><a href="#">Feb <span class="badge">13</span></a></li>
                        <li><a href="#">Mar <span class="badge">5</span></a></li>
                        <li><a href="#">Apr <span class="badge">21</span></a></li>
                        <li><a href="#">May <span class="badge">12</span></a></li>
                        <li><a href="#">Jun <span class="badge">12</span></a></li>
                      </ul>
                      <ul class="nav nav-pills nav-stacked pull-left">
                        <li><a href="#">Jul <span class="badge">19</span></a></li>
                        <li><a href="#">Aug <span class="badge">16</span></a></li>
                        <li><a href="#">Sep <span class="badge">17</span></a></li>
                        <li><a href="#">Oct <span class="badge">9</span></a></li>
                        <li><a href="#">Nov <span class="badge">13</span></a></li>
                        <li><a href="#">Dec <span class="badge">12</span></a></li>
                      </ul>
                    </div>
                    <div class="tab-pane" id="2011">
                      <ul class="nav nav-pills nav-stacked pull-left">
                        <li><a href="#">Jan <span class="badge">13</span></a></li>
                        <li><a href="#">Feb <span class="badge">22</span></a></li>
                        <li><a href="#">Mar <span class="badge">12</span></a></li>
                        <li><a href="#">Apr <span class="badge">14</span></a></li>
                        <li><a href="#">May <span class="badge">11</span></a></li>
                        <li><a href="#">Jun <span class="badge">19</span></a></li>
                      </ul>
                      <ul class="nav nav-pills nav-stacked pull-left">
                        <li><a href="#">Jul <span class="badge">16</span></a></li>
                        <li><a href="#">Aug <span class="badge">18</span></a></li>
                        <li><a href="#">Sep <span class="badge">31</span></a></li>
                        <li><a href="#">Oct <span class="badge">22</span></a></li>
                        <li><a href="#">Nov <span class="badge">23</span></a></li>
                        <li><a href="#">Dec <span class="badge">11</span></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>






<?php get_footer(); ?>
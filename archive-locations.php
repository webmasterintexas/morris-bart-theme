<?php get_header(); ?>
    <div id="header-image">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span>Locations Served by Morris Bart</span></h1>
          </div>
        </div><!-- /row -->
      </div><!-- /container -->
    </div><!-- /section-header -->
    <div class="container">
        <div class="col-sm-8 blog-summary-list">
<div class="row">
<div class="block-header">
<h2> <span class="title">Locations Served</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span></h2>
</div>
<div class="row">
<img src="http://www.morrisbart.com/wp-content/uploads/2014/05/careers.jpg" class="img-responsive" />
</div>
<p>Have you been in a vehicle accident, or the victim of a personal injury? We service several areas, find one close to you and contact us today for help with your case. </p>

</div>
<div class="row">
<button class="btn-lg btn-success"><a href="tel:8005378185" rel="nofollow" style="color:white;">Click To Call 800-537-8185 NOW!</a></button>
</div>
<hr />
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <!-- blog post -->
<div class="row">
            <div class="blog-summary">
              <h4 class="story entry-title"><span class="glyphicon glyphicon-briefcase"></span> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
              <ul class="text-muted list-inline">
                <li><i class="fa fa-calendar date updated"></i> <span class="entry-date"><?php echo get_the_date(); ?></span></li>
              </ul>
              <hr>
            </div><!-- /blog summary -->
</div>
 <?php endwhile; endif; ?>
          <!-- Pagination -->
          <ul class="pagination pull-right">
            <li class="active"><?php next_posts_link( 'Other Locations Previous' ); ?></li>
            <li class="active"><?php previous_posts_link( 'Other Locations Next' ); ?></li>
          </ul>
        </div><!-- /summary list -->
<?php get_sidebar(); ?>

    </div><!-- /container -->

<?php get_footer(); ?>
<?php get_header(); ?>
<div id="header-image">
<div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft entry-title"><span><?php the_title(); ?></span></h1>
          </div>
        </div>
</div>
    <div class="container">
      <div class="row">
        <div class="col-md-8"><!-- left main content -->
<div class="row">
    <div class="col-sm-11">
        <div class="block-header">
            <h2>
            <span class="title entry-title"><?php the_title(); ?></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            </h2>
        </div>
    </div>
</div>

<!--  begin green button -->
<div class="row visible-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT <a href="tel:8005378185" style="color:#fff; text-decoration:underline;" rel="nofollow">1-800-537-8185</a> <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>

<div class="row hidden-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT 1-800-537-8185 <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>
<!-- /green button -->
<p>Thank you for your interest in employment with Morris Bart, LLC Attorneys at Law. We value our employees and create a working environment that is professional, rewarding, comfortable and exciting.</p>
<hr />
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<span class="career-text"><?php the_content(); ?></span>
<p>Please upload your cover letter, resume and application below.</p>
<?php vfb_pro( 'id=2' ); ?>
<?php endwhile; endif; ?>
<span class="vcard author">
<span class="fn">Author: <a href="https://plus.google.com/112883956779075172119/posts"> Morris Bart </a></span></span>
</div><!-- /left main content -->
<?php get_sidebar(); ?>
    </div><!-- /container (page) -->
</div><!-- /wrapper -->
<?php get_footer(); ?>
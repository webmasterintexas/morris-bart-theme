<div class="row">
    <div class="col-md-4">
        <div class="block-header">
        <h2> <span class="title">Contact Us</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span></h2>
        </div>
        <div class="cta">
        <img src="<?php bloginfo('stylesheet_directory'); ?>/img/call-to-action.png" alt="Call Morris Bart Today!" class="cta">
        </div>
    <?php dynamic_sidebar( 'sidebar' ); ?>
    </div>
</div><!-- /row -->
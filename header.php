<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="author" content="https://plus.google.com/112883956779075172119/">
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.ico">

    <title><?php wp_title(); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('stylesheet_directory'); ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/elements.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css?rev133" />
    <link href="<?php bloginfo('stylesheet_directory'); ?>/fonts/font-awesome-4.0.3/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2423930-1']);
  _gaq.push(['_setDomainName', 'morrisbart.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<meta name="google-translate-customization" content="ab07734183820f65-968b8d7a8f9abbe0-g454246ec29d30185-12"></meta>
<?php wp_head(); ?>    
  </head>

  <body <?php body_class($class); ?>> 

<?php get_template_part('templates/menu'); ?>
    
  <div class="wrapper"> 
<?php 
//in header?
if(!is_front_page()):
	$headerimage = getCustomHeaderImage();
?>
<style>
	#header-image { margin-top: -50px; background-image:url('<?php echo $headerimage; ?>'); height:150px; background-size:cover; }
.home#header-image { margin-top: -50px; background-image:url('<?php echo $headerimage; ?>'); height:300px; background-size:cover; }
</style>
<?php endif; ?>
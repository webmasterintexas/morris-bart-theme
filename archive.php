<?php get_header(); ?>
    <div id="header-image">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span>Morris Bart Blog</span></h1>
          </div>
        </div><!-- /row -->
      </div><!-- /container -->
    </div><!-- /section-header -->
    <div class="container">
        <div class="col-sm-8 blog-summary-list">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <!-- blog post -->
<div class="row">
            <div class="blog-summary">
              <h4 class="new-story entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
              <ul class="text-muted list-inline">
                <li><i class="fa fa-user"></i><span class="vcard author"><span class="fn"> <?php the_author(); ?></span></span></li>
                <li><i class="fa fa-calendar date updated"></i> <?php the_date(); ?></li>
                <li><i class="fa fa-comments-o"></i> <?php wp_list_comments( array( 'style' => 'ul' ) ); ?></li>
              </ul>
              <hr>
              <p class="blog-text">
<?php the_post_thumbnail('thumbnail', array('class' => 'pull-right img-responsive blog-img')); ?>
                <?php the_excerpt(); ?>
              </p>
              <p class="tags">
                <a href="#"><?php the_tags(); ?></a>
              </p>

            </div><!-- /blog summary -->
</div>
 <?php endwhile; endif; ?>
          <!-- Pagination -->
          <ul class="pagination pull-right">
            <li class="active"><?php next_posts_link( 'Older posts' ); ?></li>
            <li class="active"><?php previous_posts_link( 'Newer posts' ); ?></li>
          </ul>
        </div><!-- /summary list -->
        </div>
    </div><!-- /container -->
<?php get_footer(); ?>
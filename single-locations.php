<?php get_header(); ?>

<div class="container">
  <div class="col-sm-7">
    <div class="row">
      <h2>Our
        <?php the_field('city_name'); ?>
        Personal Injury Lawyer can Help</h2>
      <?php the_post_thumbnail('thumbnail', array('class' => 'img-responsive')); ?>
      <p>Accidents that cause injuries are often unexpected, and leave you with the heavy burden of recovery, medical bills, lost wages, and pain and suffering. Let a
        <?php the_field('city_name'); ?>
        lawyer help you handle this burden, not matter what type of accident in which you have been involved. Our attorneys here at the law office are trained in handling many different types of injuries. </p>
      We provide a level of professionalism and compassion that you won’t find at other law firms. Your case will be handled both efficiently and quickly, and we fight for the maximum compensation possible for your injuries.
      </p>
      <p>When you choose our firm, you are choosing a lawyer who has the care and diligence to keep you connect and informed on what is going on in your case. We can help you to understand the process of dealing with the insurance company, and we won’t let them push you around or force you to settle for less than you deserve. Call our Baton Rouge Attorneys today at 225-341-5694 or fill out the FREE case evaluation form on our website. </p>
    </div>
    <div class="row">
      <h3>About Our
        <?php the_field('city_name'); ?>
        Personal Injury Lawyers</h3>
      <?php the_field('city_video'); ?>
    </div>
    <div class="row">
      <div itemscope itemtype="http://schema.org/Attorney"><span itemprop="name"><span class="fn org">Morris Bart</span></span>
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"> <span itemprop="streetAddress">
          <?php the_field('city_address'); ?>
          </span> <span itemprop="addressLocality">
          <?php the_field('city_name'); ?>
          </span>, <span itemprop="addressRegion">LA</span> <span itemprop="postalCode">
          <?php the_field('city_zip'); ?>
          </span> </div>
        Phone: <span itemprop="telephone"> <span class="tel">
        <?php the_field('city_phone'); ?>
        </span> </span>
        <?php the_field('city_map_url'); ?>
      </div>
    </div>
    <div class="row">
      <h3>Any day of the week, no matter the hours! 24/7/365</h3>
      <div class="table-responsive">
        <table class="table">
          <tr class="success">
            <td>Monday</td>
            <td>8:00 AM – 5:00 PM</td>
          </tr>
          <tr class="success">
            <td>Tuesday</td>
            <td>8:00 AM – 5:00 PM</td>
          </tr>
          <tr class="success">
            <td>Wednesday</td>
            <td>8:00 AM – 5:00 PM</td>
          </tr>
          <tr class="success">
            <td>Thursday</td>
            <td>8:00 AM – 5:00 PM</td>
          </tr>
          <tr class="success">
            <td>Friday</td>
            <td>8:00 AM – 5:00 PM</td>
          </tr>
        </table>
      </div>
    </div>
    <div class="row">
      <?php the_field('city_map'); ?>
    </div>
    <h3>How our Lawyer / Attorney Can Help</h3>
    <div class="row">Our law office has office locations all over the state, but we have an office located near you. Having a
      <?php the_field('city_name'); ?>
      lawyer to help you with your case is crucial to staying informed and included in the process of the case each step of the way. Our skilled attorneys are equipped to give you the highest quality of service, and the best advice on what you should do in your case. Each case is different, but nothing is too big or too small for an attorney at our office.</div>
    <div class="row">
      <h3>Lawsuit, Claims &amp; Settlements.</h3>
      An attorney at our office is ready to take on any case. If you have been injured and you were not at fault, let us give you the best legal advice and representation that you can get. We handle a wide variety of personal injuries including, but not limited to:
      <ul>
        <li>Catastrophic Injuries – If you have sustained an accident that will require lifelong care and expenses or is physically unrepairable, contact our office today. An attorney at our office will go through every single detail of your case, to determine the best plan of action and get you the most compensation possible.</li>
        <li>Medical Malpractice – We all like to believe that healthcare professionals are mistake-proof. The unfortunate truth is that they are not. If you or someone you know has been injured due to neglect, a misdiagnosis, or mistreatment by a healthcare professional, put our office to work for you today.</li>
        <li>Wrongful Death – The death of a loved one is always difficult, but when it is unexpected and premature, that loss can seem impossible to deal with. If someone you love has died at the fault of someone else, call your lawyer now. An attorney at our law firm will treat you and your case with compassion and diligence in order to get you the results and closure you need as quickly as possible.</li>
        <li>Product Liability – Injuries caused by malfunctioning machinery or improperly designed products happen all the time. If you or someone you know has been injured due to a product or medical device recall or malfunction, call Morris Bart today.</li>
        <li>Auto Accidents – Automobile accidents happen in the blink of an eye, and they literally change your entire life. You have the added financial stresses along with your pain and suffering. Whether it’s a car wreck, truck wreck or any other type of automobile wreck, call Morris Bart today. One of our lawyers is standing by to help you.</li>
        <li>Premises Liability – If you or someone you know has been injured, and you feel that it was due to poorly maintained premises or grounds, then call Morris Bart today to see if you have a case. The consultation is free, and our team of skilled attorneys will look into your case right away.</li>
      </ul>
    </div>
    <!-- /row -->
    <div class="row">
      <h2 style="text-align: left;">Lawyers for Personal Injuries</h2>
    </div>
    <div class="row">If you have been injured in any type of
      <?php the_field('city_name'); ?>
      accident, your first priority is your health and safety. Seek medical attention immediately, and then call Morris Bart. We will work with you, give you the legal advice you need, and fight hard to get you the maximum level of compensation. Call
      <?php the_field('city_phone'); ?>
      or fill out the FREE case evaluation form on our website for more information on how our
      <?php the_field('city_name'); ?>
      personal injury lawyers can help you.</div>
    <div class="row">
      <?php if(have_posts()): while(have_posts()): the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; endif; ?>
    </div>
    <div class="row">
      <div class="col-sm-3 col-sm-offset-2">
        <button class="btn btn-success btn-lg">Contact Us NOW!</button>
      </div>
    </div>
    <table class="table table-striped table-responsive">
      <tbody>
        <tr>
          <td colspan="2" align="center"><div align="center">
              <h3>
                <?php the_field('city_name'); ?>
                Near By Cities</h3>
            </div></td>
        </tr>
        <tr>
          <td align="left" valign="top" width="50%"><ul>
              <li>
                <?php the_field('nearby_city_one'); ?>
              </li>
              <li>
                <?php the_field('nearby_city_two'); ?>
              </li>
              <li>
                <?php the_field('nearby_city_three'); ?>
              </li>
            </ul></td>
          <td align="left" valign="top" width="50%"><ul>
              <li>
                <?php the_field('nearby_city_four'); ?>
              </li>
              <li>
                <?php the_field('nearby_city_five'); ?>
              </li>
              <li>
                <?php the_field('nearby_city_six'); ?>
              </li>
            </ul></td>
        </tr>
        <tr>
          <td align="left" valign="top"></td>
          <td align="left" valign="top"></td>
          <td align="left" valign="top"></td>
        </tr>
        <tr>
          <td colspan="2" align="center" valign="middle"><div align="center">
              <h3>Near By Zip Codes</h3>
            </div></td>
        </tr>
        <tr>
          <td align="left" valign="top"><ul>
              <li>
                <?php the_field('nearby_zip_four'); ?>
              </li>
              <li>
                <?php the_field('nearby_zip_five'); ?>
              </li>
              <li>
                <?php the_field('nearby_zip_six'); ?>
              </li>
            </ul></td>
          <td align="left" valign="top"><ul>
              <li>
                <?php the_field('nearby_zip_four'); ?>
              </li>
              <li>
                <?php the_field('nearby_zip_five'); ?>
              </li>
              <li>
                <?php the_field('nearby_zip_six'); ?>
              </li>
            </ul></td>
        </tr>
      </tbody>
    </table>
<p><a href="https://plus.google.com/112883956779075172119/" rel="author">Google +</a></p>
  </div>

  <!-- /colsm7 -->
  <?php get_sidebar(); ?>
</div>
<!-- /container -->
<?php get_footer(); ?>

<?php

// Responsive Menu
include('wp_bootstrap_navwalker.php');


add_action( 'after_setup_theme', 'register_my_menus' );
 
function register_my_menus() {
	register_nav_menus( array(
		'primary' => __( 'main-menu', 'Main Menu' ),
	) );
}

// Register Sidebar
function sidebar() {

	$args = array(
		'id'            => 'sidebar',
		'name'          => __( 'Sidebar', 'morrisbart' ),
		'description'   => __( 'The Main Sidebar', 'morrisbart' ),
	);
	register_sidebar( $args );

}

// Hook into the 'widgets_init' action
add_action( 'widgets_init', 'sidebar' );

// Add image thumbnail support
add_theme_support( 'post-thumbnails' );

//Custom header images
function getCustomHeaderImage(){
 
	global $post;
	
	$headerimage = get_bloginfo('stylesheet_directory').'/img/header-images/new-orleans.jpg';
         if(is_post_type_archive('careers')){
              $headerimage = 'http://morrisbart.com/wp-content/themes/lawyersite3/img/header-images/careers.jpg';
           }	
	 elseif(is_archive()){
              $headerimage = 'http://morrisbart.com/wp-content/themes/lawyersite3/img/header-images/blog.jpg';
           }
         elseif(is_category()){
              $headerimage = 'http://morrisbart.com/wp-content/themes/lawyersite3/img/header-images/blog.jpg';
           }
          elseif(is_page()){
              $cImg = get_post_meta( $post->ID, 'headerimage', true );
                 if(!empty($cImg)){
              $headerimage = get_bloginfo('stylesheet_directory').'/'.$cImg;
         }
 }
 
 return $headerimage;

	
}

//limit admin search to titles only
add_filter( 'posts_search', 'wpse_45153_filter_search', null, 2 );
function wpse_45153_filter_search( $search, $a_wp_query ) {
    if ( !is_admin() ) return $search; // work only in the dashboard

    $search = preg_replace( "# OR \(.*posts\.post_content LIKE \\'%.*%\\'\)#", "", $search );

    return $search;
}

//hentry errors fix
function add_suf_hatom_data($content) {
    $t = get_the_modified_time('F jS, Y');
    $author = get_the_author();
    $title = get_the_title();
if (is_home() || is_singular() || is_archive() ) {
        $content .= '<div class="hatom-extra" style="display:none;visibility:hidden;"><span class="entry-title">'.$title.'</span> was last modified: <span class="updated"> '.$t.'</span> by <span class="author vcard"><span class="fn">'.$author.'</span></span></div>';
    }
    return $content;
}
<!-- Foooter
================== -->
</div>
  <footer>
    <div class="container">
      <div class="row">
        <!-- Contact Us 
        =================  -->
        <div class="col-sm-4">
          <div class="headline"><h3>Contact us</h3></div>
          <div class="content">
            <?php get_template_part('templates/footer-contact'); ?>
          </div>
        </div>
        <!-- Social icons 
        ===================== -->
        <div class="col-sm-4">
          <div class="headline"><h3>Go Social</h3></div>
          <div class="content social">
            <p>Stay in touch with us:</p>
            <ul>
                <li><a href="https://twitter.com/MorrisBartLLC" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li><a href="https://www.facebook.com/morrisbartllc" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li><a href="https://www.pinterest.com/morrisbart/" target="_blank"><i class="fa fa-pinterest"></i></a>
                </li>
                <li><a href="https://www.youtube.com/channel/UCZOqvhHrXdd0P1Lldl6FTMw" target="_blank"><i class="fa fa-youtube"></i></a>
                </li>
                <li><a href="http://www.linkedin.com/title/attorney/at-morris+bart+llc/" target="_blank"><i class="fa fa-linkedin"></i></a>
                </li>
                <li><a href="https://plus.google.com/114696260676388147937/posts" rel="publisher" target="_blank"><i class="fa fa-google-plus"></i></a>
                </li>
                <li><a href="<?php if(is_page('182')){ ?>
https://plus.google.com/b/115091113322685916113/115091113322685916113/
<?php } else { ?> https://plus.google.com/112883956779075172119/posts <?php } ?>" rel="author" target="_blank"><i class="fa fa-google-plus"></i></a>
                </li>
<li><a href="http://feeds.feedburner.com/MorrisBartAndAssociates" target="_blank"><i class="fa fa-rss-square"></i></a></li>
            </ul>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- Subscribe 
        =============== -->
        <div class="col-sm-4">
          <div class="headline"><h3>Subscribe</h3></div>
          <div class="content">
            <p>Enter your e-mail below to subscribe to our free newsletter.<br />We promise not to bother you often!</p>
            <form class="form" role="form">
              <div class="row">
                <div class="col-sm-8">
                  <div class="input-group">
                    <label class="sr-only" for="subscribe-email">Email address</label>
                    <input type="email" class="form-control" id="subscribe-email" placeholder="Enter email">
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-default">OK</button>
                    </span>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- Legal 
  ============= -->
  <div class="legal">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <p>&copy; Morris Bart Attorneys and Trial Lawyers <?php echo date('Y'); ?> <a href="/privacy-policy">Privacy Policy</a> | <a href="/sitemap/">Sitemap</a> | Author <a href="
<?php if(is_page('182')){ ?>
https://plus.google.com/b/115091113322685916113/115091113322685916113/
<?php } else { ?> https://plus.google.com/112883956779075172119/posts <?php } ?>" rel=author">Morris Bart</a> | <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT, gaTrack: true, gaId: 'UA-2423930-1'}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script></p>
        </div>
      </div>
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-briefcase"></i> LET US HELP WITH YOUR CASE</h4>
      </div>
      <div class="modal-body">
        <?php vfb_pro( 'id=1' ); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script src="http://www.apexchat.com/scripts/invitation.ashx?company=morrisbart"></script>
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/custom.js"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/scrolltopcontrol.js"></script><!-- Scroll to top javascript -->
    <?php wp_footer(); ?>
  </body>
</html>
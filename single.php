<?php get_header(); ?>
<div id="header-image">
<div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft entry-title"><span><?php the_title(); ?></span></h1>
          </div>
        </div>
</div>
    <div class="container">
      <div class="row">
        <div class="col-md-8"><!-- left main content -->
<div class="row">
    <div class="col-sm-11">
        <div class="block-header">
            <h2>
            <span class="title entry-title"><?php the_title(); ?></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            </h2>
        </div>
    </div>
</div>

<!--  begin green button -->
<div class="row visible-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT <a href="tel:8005378185" style="color:#fff; text-decoration:underline;" rel="nofollow">1-800-537-8185</a> <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>

<div class="row hidden-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT 1-800-537-8185 <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>
<!-- /green button -->
<hr />
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<span class="vcard author">
<span class="fn">Author: <a href="https://plus.google.com/112883956779075172119/posts"> Morris Bart </a></span></span><br />
<i class="fa fa-calendar"></i> <span class="post_date date updated"><?php the_time('j F,Y'); ?></span>
<?php endwhile; endif; ?>
</div><!-- /left main content -->
<?php get_sidebar(); ?>
</div><!-- /container (page) -->
</div><!-- /wrapper -->
<?php get_footer(); ?>
<?php get_header(); ?>
<?php get_header(); ?>
<div id="header-image">
<div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span><?php the_title(); ?></span></h1>
          </div>
        </div>
</div>
<div class="container">

<div class="col-sm-8">
<div class="row">
    <div class="col-sm-11">
        <div class="block-header">
            <h2>
            <span class="title"><?php echo get_post_meta( $post->ID, 'h2', true );?></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            </h2>
        </div>
    </div>
</div>

<?php if(have_posts()): while(have_posts()): the_post(); ?>

<?php the_content(); ?>

<?php endwhile;endif; ?>

</div>
<div class="row">
<?php get_sidebar(); ?>
</div>
</div>

<?php get_footer(); ?>
<?php get_header(); ?>
<div id="header-image">
<div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span><?php the_title(); ?></span></h1>
          </div>
        </div>
</div>
    <div class="container">
      <div class="row">
        <div class="col-md-8"><!-- left main content -->

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<div class="row">
    <div class="col-sm-11">
        <div class="block-header">
            <h2>
            <span class="title"><?php echo get_post_meta( $post->ID, 'h2', true );?></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            <span class="decoration hidden-xs"></span>
            </h2>
        </div>
    </div>
</div>

<!--  begin green button -->
<div class="row visible-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT <a href="tel:<?php  the_field('phone_number'); ?>" style="color:#fff; text-decoration:underline;" rel="nofollow"><?php  the_field('phone_number_readable'); ?></a> <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>

<div class="row hidden-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT <?php  the_field('phone_number_readable'); ?> <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>
<!-- /green button -->
<?php the_content(); ?>

<?php endwhile; endif; ?>
Author: <a href="<?php if(is_page('182')){ ?> https://plus.google.com/b/115091113322685916113/115091113322685916113 <?php } else { ?> https://plus.google.com/112883956779075172119/posts <?php } ?>"> Morris Bart </a>
</div><!-- /left main content -->
<?php get_sidebar(); ?>
    </div><!-- /container (page) -->
</div><!-- /wrapper -->
<?php get_footer(); ?>
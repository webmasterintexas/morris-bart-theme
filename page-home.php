<?php get_header(); ?>


<!-- Showcase
    ================ -->
    <?php get_template_part('templates/slider'); ?>
    
    <div class="container">
      <!-- Services
        ================ -->
      <?php get_template_part('templates/services'); ?>
      
      <div class="row">
        <!-- Welcome message
            ================= -->
        <div class="col-md-8">
        <div class="block-header">
          <h2>
            <span class="title">Louisiana Personal Injury Lawyer</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span>
          </h2>
        </div>

<!--  begin green button -->
<div class="row visible-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT <a href="tel:<?php  the_field('phone_number'); ?>" style="color:#fff; text-decoration:underline;" rel="nofollow"><?php  the_field('phone_number_readable'); ?></a> <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>

<div class="row hidden-xs">
<div class="col-sm-8">
<div class="btn-lg btn-success"><h3>CALL US NOW AT <?php  the_field('phone_number_readable'); ?> <span class="glyphicon glyphicon-earphone"></span></h3>
</div>
</div>
</div>
<!-- /green button -->

          <?php if(have_posts()): while(have_posts()): the_post(); ?>
          <?php the_content(); ?>
          <?php endwhile; endif; ?>
        </div>
        <!-- Last updated -->
        
<?php get_sidebar(); ?>
      </div>
      
      <!-- Recent Works
        =================== -->
      <div class="row">
        <div class="col-md-12 block-header">
          <h2>
            <span class="title">Testimonials</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span>
          </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php // bloginfo('stylesheet_directory'); ?>/img/test-warren-i.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/WWMOM6P70LA?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Warren I</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i>
                </div>
              <p>Morris Bart did a great job on my accident case.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php bloginfo('stylesheet_directory'); ?>/img/works2.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/Jc-kYOX2uMY?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Tonia H</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                </div>
              <p>I would highly recommend Morris Bart to anyone who needs a lawyer</p>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm"></div>
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php bloginfo('stylesheet_directory'); ?>/img/works3.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/vDHhOgt_7Ao?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Stephanie G</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star-half"></i>
                </div>
              <p>I'm very very happy with my settlement.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php bloginfo('stylesheet_directory'); ?>/img/works4.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/uNBCOPrmiFU?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Augusta B</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i>
                </div>
              <p>I am very satisfied with the services rendered at Morris Bart.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<?php get_footer(); ?>
<?php get_header(); ?>
    <div id="header-image">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <!-- Remove the .animated class if you don't want things to move -->
            <h1 class="animated slideInLeft"><span>Career Opportunities at Morris Bart</span></h1>
          </div>
        </div><!-- /row -->
      </div><!-- /container -->
    </div><!-- /section-header -->
    <div class="container">
        <div class="col-sm-8 blog-summary-list">
<div class="row">
<div class="block-header">
<h2> <span class="title">Career Opportunities</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span></h2>
</div>
<div class="row">
<img src="http://www.morrisbart.com/wp-content/uploads/2014/05/careers.jpg" class="img-responsive" />
</div>
<p>Thank you for your interest in employment with Morris Bart, LLC Attorneys at Law. We value our employees and create a working environment that is professional, rewarding, comfortable and exciting.</p>
<p>Below is a list of our current openings; however, we are always interested in hearing from qualified candidates so we encourage you to submit your resume.</p>
<p>Before your resume will be considered, please fill out and send in our <i class="fa fa-file-pdf-o fa-2x"></i>  <a href="http://www.morrisbart.com/wp-content/themes/lawyersite3/img/Application_for_employment.pdf">  Employment Application (PDF)</a>.</p>
</div>
<div class="row">
<button class="btn-lg btn-success"><a href="tel:8005378185" rel="nofollow" style="color:white;">Click To Call 800-537-8185 NOW!</a></button>
</div>
<hr />
<?php if(have_posts()): while(have_posts()): the_post(); ?>
          <!-- blog post -->
<div class="row">
            <div class="blog-summary">
              <h4 class="story entry-title"><span class="glyphicon glyphicon-briefcase"></span> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
              <ul class="text-muted list-inline">
                <li><i class="fa fa-calendar date updated"></i> <span class="entry-date"><?php echo get_the_date(); ?></span></li>
              </ul>
              <hr>
            </div><!-- /blog summary -->
</div>
 <?php endwhile; endif; ?>
          <!-- Pagination -->
          <ul class="pagination pull-right">
            <li class="active"><?php next_posts_link( 'Older Careers' ); ?></li>
            <li class="active"><?php previous_posts_link( 'Newer Careers' ); ?></li>
          </ul>
        </div><!-- /summary list -->
<?php get_sidebar(); ?>

    </div><!-- /container -->

<?php get_footer(); ?>
<?php get_header(); ?>


<!-- Showcase
    ================ -->
    <div id="wrap">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <h1 class="animated slideInDown">Attorneys and Trial Lawyers</h1>
            <div class="list">
              <ul>
                <li class="animated slideInLeft first"><span><i class="fa fa fa-code"></i> Car Accidents </span></li>
                <li class="animated slideInLeft second"><span><i class="fa fa-cogs"></i>   Truck Accidents </span></li>
                <li class="animated slideInLeft third"><span><i class="fa fa-tablet"></i>  Work Injuries </span></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 hidden-sm hidden-xs">
            <div class="showcase">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/img/Morris-Bart.png" alt="Morris Bart" class="iMac animated fadeInDown">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <!-- Services
        ================ -->
      <div class="row">
        <div class="col-md-12">
          <div class="services">
            <ul>
                <li>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/avvo.png" class="img-responsive">
                </li>
                <li>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/best2012.png" class="img-responsive">
                </li>
                <li>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/lil.png" class="img-responsive">
                </li>
                <li>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/atl.png" class="img-responsive">
                </li>
                <li>
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/img/milliondollar.png" class="img-responsive">
                </li>
            </ul>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Welcome message
            ================= -->
        <div class="col-md-8">
        <div class="block-header">
          <h2>
            <span class="title">Louisiana Personal Injury Lawyer</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span>
          </h2>
        </div>
          <img src="<?php bloginfo('stylesheet_directory'); ?>/img/about.jpg" class="img-about img-responsive" alt="About">
          <p>
            Welcome to Morris Bart, Attorneys at Law where serious experience, professionalism, and personal attention get serious results. This firm is committed to protecting your legal right to be compensated for the injuries you have suffered, whether in an auto accident, motorcycle accident, or slip and fall accident.  
            <br /><br />
            Morris Bart and his team of associates have vast knowledge and experience in handling all types of claims for clients who have been injured.  You can trust the lawyers at Morris Bart to handle your case aggressively to maximize your recovery while also maintaining the level of privacy and dignity you deserve. 
          </p>
          <div class="info-board info-board-blue">
            <h4>Personal Injury Accident?</h4>
            <p>If you have had a personal injury accident, contact a lawyer at Morris Bart today. Time is of the essence.</p>
          </div>
<div class="row">
	<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
</div>
        </div><!-- /left content  -->
        
<?php get_sidebar(); ?>
      </div>
      
      <!-- Recent Works
        =================== -->
      <div class="row">
        <div class="col-md-12 block-header">
          <h2>
            <span class="title">Testimonials</span><span class="decoration"></span><span class="decoration"></span><span class="decoration"></span>
          </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php // bloginfo('stylesheet_directory'); ?>/img/test-warren-i.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/WWMOM6P70LA?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Warren I</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i>
                </div>
              <p>Morris Bart did a great job on my accident case.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php bloginfo('stylesheet_directory'); ?>/img/works2.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/Jc-kYOX2uMY?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Tonia H</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                </div>
              <p>I would highly recommend Morris Bart to anyone who needs a lawyer</p>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm"></div>
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php bloginfo('stylesheet_directory'); ?>/img/works3.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/vDHhOgt_7Ao?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Stephanie G</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star-half"></i>
                </div>
              <p>I'm very very happy with my settlement.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="thumbnail">
            <!--img src="<?php bloginfo('stylesheet_directory'); ?>/img/works4.jpg" class="img-responsive" alt="..."-->
            <iframe width="240" height="190" src="//www.youtube.com/embed/uNBCOPrmiFU?rel=0" frameborder="0" allowfullscreen></iframe>
            <!--div class="visit"><a href="#"><i class="fa fa-question-circle"></i> More details...</a></div-->
            <div class="caption">
              <h4>Augusta B</h4>
                <div class="rating">
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i> 
                  <i class="fa fa-star"></i>
                </div>
              <p>I am very satisfied with the services rendered at Morris Bart.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<?php get_footer(); ?>